from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

"""Run the stations_by_distance function in geo.py then prints the 10 furthest and 10 closest stations from cambridge city centre"""

def run():
    stations = build_station_list()
    closest_station = stations_by_distance(stations,(52.2053, 0.1218))[:10]
    furthest_station = stations_by_distance(stations,(52.2053, 0.1218))[-10:]

    def town_tuple(stations,x):
        for station in stations:
            if x[0] == station.name:
                if station.town != None:
                    return (x[0], station.town, x[1])
                else:
                    return x
   
    town_list = []
    for x in closest_station:
        town_list.append(town_tuple(stations,x))
    print(town_list)

    for x in furthest_station:
        town_list.append(town_tuple(stations,x))
    print(town_list)

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

run()