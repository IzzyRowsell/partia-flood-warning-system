from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key
from floodsystem.analysis import polyfit
import datetime

"""for each of the 5 stations at which the current relative water level is greatest and for a time period extending back 2 days, 
plots the level data and the best-fit polynomial of degree 4 against time. 
Show the typical range low/high on your plot."""

def run():
    stations = build_station_list()
    update_water_levels(stations)
    station_list = []
    for station in stations:
        if station.latest_level != None:
            station_list.append((station, station.latest_level))
    top_list = sorted_by_key(station_list,1,reverse=True)[:5]

    dt = 10
    for station in top_list:
        dates, levels = fetch_measure_levels(station[0].measure_id, dt=datetime.timedelta(days=dt))
        p, time_shift = polyfit(dates, levels, 4)
        plot_water_level_with_fit(station[0], dates, levels, p)



if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

run()