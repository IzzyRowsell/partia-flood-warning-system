from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key 
import datetime


"""plots the water levels over the past 10 days for the 5 stations at which the current relative water level is greatest."""


def run():
    stations = build_station_list()
    update_water_levels(stations)
    station_list = []
    for station in stations:
        if station.latest_level != None:
            station_list.append((station, station.latest_level))
    top_list = sorted_by_key(station_list,1,reverse=True)[:5]

    dt = 10
    for station in top_list:
        dates, levels = fetch_measure_levels(station[0].measure_id, dt=datetime.timedelta(days=dt))
        plot_water_levels(station[0], dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

run()
