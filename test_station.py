# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

# Create stations for testing
s_id = "test-s-id"
m_id = "test-m-id"
label = "test station 1"
coord = (2, 3)
trange = (1,2)
river = "River X"
town = "Town 1"
s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 2"
trange = (2,3)
t = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 3"
trange = None
u = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 4"
trange = (-4,-20)
v = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 5"
trange = (17, 0)
w = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 6"
trange = (3,3)
x = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

def test_typical_range_consistent():

    #normal output
    a = MonitoringStation.typical_range_consistent(s)

    #normal output
    b = MonitoringStation.typical_range_consistent(t)

    #None value for trange
    c = MonitoringStation.typical_range_consistent(u)

    #negative values
    d = MonitoringStation.typical_range_consistent(v)

    #low range > high range
    e = MonitoringStation.typical_range_consistent(w)

    #low range = high range
    f = MonitoringStation.typical_range_consistent(x)

    assert a == True
    assert b == True
    assert c == False
    assert d == False
    assert e == False
    assert f == True


