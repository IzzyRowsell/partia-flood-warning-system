import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime, timedelta
import numpy as np


"""Plot_water_levels will output a graph of:
water level data against time for a station, and include on the plot lines for the typical low and high levels. 
The axes should be labelled and use the station name as the plot title. The function should have the signature:"""

def plot_water_levels(station, dates, levels):
    Typ_low = [station.typical_range[0]] * len(levels)
    Typ_high = [station.typical_range[1]] * len(levels)

# Plot
    plt.plot(dates, levels, label='Actual Level')
    plt.plot(dates, Typ_low, label='Typical Low Level')
    plt.plot(dates, Typ_high, label='Typical High Level')

# Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.legend()
    plt.xticks(rotation=45);
    plt.title("Station {} ".format(station.name))
    

# Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()

""" function which plots the water level data and the best-fit polynomial"""
def plot_water_level_with_fit(station, dates, levels, p):
    Typ_low = [station.typical_range[0]] * len(levels)
    Typ_high = [station.typical_range[1]] * len(levels)
    x= matplotlib.dates.date2num(dates)
    y = levels
    print(len(x))
# Plot

    plt.plot(dates, y, '.', label='Actual Level')
    plt.plot(dates, Typ_low, label='Typical Low Level')
    plt.plot(dates, Typ_high, label='Typical High Level')
    x1 = np.linspace(x[0], x[-1], len(x))
    plt.plot(dates, p(x1 - x[0]), label='Polynomial Fit')



# Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.legend()
    plt.xticks(rotation=45);
    plt.title("Station {} ".format(station.name))
    

# Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()