"""This module contains a collection of functions related to
geographical data."""


from .utils import sorted_by_key  # noqa
#from haversine import haversine, Unit


"""Function which takes a list of station objects and a coordinate p, then returns
(station, distance) tuple where distance is a float type -- distance of monitoring station from p 
List should be sorted by distance"""

def haversine(a,b):
    return ((b[1]-a[1])**2 +(b[0]-a[0])**2)**0.5

def stations_by_distance (stations, p):
    distance = []
    for station in stations:
        c = station.coord
        distance.append((station.name, round((haversine(c,p)),5)))
    return sorted_by_key(distance,1)

"""Function which retuns a list of monitoring station NAMES which are in a radius rkm of a geographical coordinate x"""
def stations_within_radius(stations, centre, r):
    station_list = []
    for station in stations:
        if haversine(station.coord, centre) <= abs(r):
            station_list.append(station.name)
    station_list.sort()
    return station_list

"""Function which takes input of stations and returns a set with names of rivers with a monitoring station
Duplication of rivers removed"""

def rivers_with_station(stations):
    river_list = []
    for station in stations:
        if station.river != None and station.river not in river_list:
            river_list.append(station.river)
    return river_list

""" function which retuns a dict that maps river names to a list of stations on a given river"""
def stations_by_river(stations):
    river_dict = {}
    for station in stations:
        if station.river != None and station.river not in river_dict:
            river_dict[station.river] = [station.name]
        elif station.river != None:
            river_dict[station.river].append(station.name)
    return river_dict

"""function that returns N rivers with the greatest number of monitoring stations"""
def rivers_by_station_number(stations, N):
    river_stations_dict = {}
    river_stations_list = []
    for station in stations:
        if station.river not in river_stations_dict:
            river_stations_dict[station.river] = [station.name]
        else:
            river_stations_dict[station.river].append(station.name)
    for river in river_stations_dict.keys(): 
        river_stations_list.append((river, len(river_stations_dict[river])))
    sorted_list = sorted_by_key(river_stations_list, 1, reverse=True)
    count = 0
    for i in range((len(sorted_list) - N)):
        if sorted_list[N][1] == sorted_list[N+i][1]:
            count += 1
    return sorted_list[:N + count]