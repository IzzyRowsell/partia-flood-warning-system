import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import datetime


"""water level time history (dates, levels) for a station 
computes a least-squares fit of a polynomial of degree p to water level data. 
The function should return a tuple of (i) the polynomial object and (ii) any shift of the time (date) axis (see below). """

def polyfit(dates, levels, p):
    if type(dates[1]) == datetime.datetime:
        x= matplotlib.dates.date2num(dates)
    else:
        x = dates

    y = levels

# Using shifted x values, find coefficient of best-fit
# polynomial f(x) of degree 4
    p_coeff = np.polyfit(x - x[0], y, 4)

# Convert coefficient into a polynomial that can be evaluated,
# e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return (poly, x[0])