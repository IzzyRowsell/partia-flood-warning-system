from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import build_station_list


def stations_level_over_threshold(stations, tol):
    stations_list = []

    for i in range(len(stations)):
        if MonitoringStation.relative_water_level(stations[i]) == None:
            pass
        elif MonitoringStation.relative_water_level(stations[i]) <= tol:
            pass
        else:
            stations_list.append((stations[i].name, MonitoringStation.relative_water_level(stations[i])))

    sorted_stations_over_threshold = sorted_by_key(stations_list, 1)

    for i in range(len(stations)):
        if MonitoringStation.relative_water_level(stations[i]) == None:
           sorted_stations_over_threshold.append((stations[i].name, "Error in data"))

    return sorted_stations_over_threshold



def stations_highest_rel_level(stations, N):
    stations_list = []

    for i in range(len(stations)):
        if MonitoringStation.relative_water_level(stations[i]) == None:
            pass
        else:
            stations_list.append((stations[i].name, MonitoringStation.relative_water_level(stations[i])))

    sorted_stations_by_rel_level = sorted_by_key(stations_list, 1, True)
    list_of_N = sorted_stations_by_rel_level[:N]

    return list_of_N

