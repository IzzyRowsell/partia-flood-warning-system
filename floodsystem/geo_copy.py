"""This module contains a collection of functions related to
geographical data."""


from .utils import sorted_by_key  # noqa
from haversine import haversine, Unit


"""Function which takes a list of station objects and a coordinate p, then returns
(station, distance) tuple where distance is a float type -- distance of monitoring station from p 
List should be sorted by distance"""

def stations_by_distance (stations, p):
    distance = []
    for station in stations:
        c = station.coord
        distance.append((station.name, round((haversine(c,p)),5)))
    return sorted_by_key(distance,1)

"""Function which retuns a list of monitoring station NAMES which are in a radius rkm of a geographical coordinate x"""
def stations_within_radius(stations, centre, r):
    station_list = []
    for station in stations:
        if haversine(station.coord, centre) <= r:
            station_list.append(station.name)
    station_list.sort()
    return station_list

"""Function which takes input of stations and returns a set with names of rivers with a monitoring station
Duplication of rivers removed"""

def rivers_with_station(stations):
    river_list = []
    for station in stations:
        if station.river not in river_list:
            river_list.append(station.river)
    return river_list

""" function which retuns a dict that maps river names to a list of stations on a given river"""
def stations_by_river(stations):
    river_dict = {}
    for station in stations:
        if station.river not in river_dict:
            river_dict[station.river] = [station.name]
        else:
            river_dict[station.river].append(station.name)
    return river_dict

""" function that returns N rivers with the greatest number of """