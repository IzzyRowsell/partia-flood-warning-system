from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.station import MonitoringStation

s_id = "test-s-id"
m_id = "test-m-id"
label = "some station"
coord = (-2.0, 4.0)
trange = (3,6)
river = "River X"
town = "My Town"
s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
d1 = [0,1,2,3,4,5,6,7,8,9]
l1 = [0,1,2,3,4,5,6,7,8,9]

#tests to see the module can be succesfully called
def test_plot_water_levels():
    x = plot_water_levels(s, d1, l1)



#tests to see the module can be succesfully called
def test_plot_water_level_with_fit():
    x = plot_water_level_with_fit(s, d1, l1, p)
