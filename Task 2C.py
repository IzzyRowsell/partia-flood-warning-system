from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

"Run the stations_highest_rel_level in flood and get the 10 stations with highest relative level"

def run():
    stations = build_station_list()
    update_water_levels(stations)
    print(stations_highest_rel_level(stations, 10))
    return

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")

run()