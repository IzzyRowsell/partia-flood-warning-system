from floodsystem.stationdata import build_station_list, inconsistent_typical_range_station

"""Run the stations_by_distance function in geo.py then prints the 10 furthest and 10 closest stations from cambridge city centre"""

def run():
    stations = build_station_list()
    print(sorted(inconsistent_typical_range_station(stations)))
    return

#checks the output since it updates
def run_2():
    stations = build_station_list()
    for station in stations:
        if station.typical_range == None:
            None
        elif station.typical_range[0] > station.typical_range[1]:
            print(station.typical_range[0], station.typical_range[1])
            print(station.name)

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

run()
#run_2()
