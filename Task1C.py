from floodsystem.geo_copy import stations_within_radius
from floodsystem.stationdata import build_station_list

"""Run the stations_by_distance function in geo.py then prints the 10 furthest and 10 closest stations from cambridge city centre"""

def run():
    stations = build_station_list()
    print(stations_within_radius(stations, (52.2053, 0.1218), 10))
    return

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

run()