from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.station import MonitoringStation

"""Unit Test for the functions in the geo module"""

# Create stations for testing
s_id = "test-s-id"
m_id = "test-m-id"
label = "test station 1"
coord = (2, 3)
trange = (-2.3, 3.4445)
river = "River X"
town = "Town 1"
s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 2"
coord = (3, 4)
river = "River Y"
t = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 3"
coord = (4, 5)
river = None
u = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 4"
river = "River X"
v = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

def test_stations_by_distance():
    station_list = [u, t, s]
    #test haversine function
    a = stations_by_distance(station_list, (1,1))

    #test sorted by key function works
    b = stations_by_distance(station_list, (0,0))

    #test wrong coordinate format
    c = stations_by_distance(station_list, (1,1,0))

    return a[1]

    assert a[0][1] ==  2.23607
    assert b[0][0] == "test station 1"
    assert b[1][0] == "test station 2"
    assert b[2][0] == "test station 3"
    assert c[0][1] == 2.23607


def test_stations_within_radius():
    station_list = [u, t, s]
    #tests normal output
    a = stations_within_radius(station_list, (0,0), 4)

    #tests no output
    b = stations_within_radius(station_list, (0,0), 1)

    #tests a negative radius
    c = stations_within_radius(station_list, (0,0), -4)

    assert a == ["test station 1"]
    assert b == []
    assert c == ["test station 1"]


def test_rivers_with_station():
    station_list = [u, t, s]

    #tests normal list with No rivers and rivers
    a = rivers_with_station(station_list)
    station_list.append(v)

    #tests an input with same river name
    b = rivers_with_station(station_list) 

    #tests an input list with no river names
    c = rivers_with_station([u,u])

    assert a == ['River Y', 'River X']
    assert b == ['River Y', 'River X']
    assert c == []



def test_stations_by_river():

#tests normal input of stations with and without rivers
    station_list = [s,t,u]
    a=stations_by_river(station_list)

#tests an input where two stations share a river
    station_list.append(v)
    b=stations_by_river(station_list)

#tests a list with no river names
    c=stations_by_river([u,u])

    #assert a == {'River X': ['test station 1'], 'River Y': ['test station 2']}
    assert b == {'River X': ['test station 1', 'test station 4'], 'River Y': ['test station 2']}
    assert c == {}

    print(b)

test_stations_by_river()


"""def test_rivers_by_station_number():
    station_list = [s,t,u,s,s,v,t,u]
    #tests normal inputs for the function
    a = rivers_by_station_number(station_list,2)
    
    station_list = [s,t,u,v,v]
    #tests normal inputs for the function
    b = rivers_by_station_number(station_list, 1)

    assert a == [('River X', 4), ('River Y', 2)]
    assert b == [('River X', 3)]

test_rivers_by_station_number()"""