
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

"Run the relative_water_level function in MonitoringStation and stations_level_over_threshold in flood and print the name and level of each station with level over 0.8"


def run():
    stations = build_station_list()
    update_water_levels(stations)
    print(stations_level_over_threshold(stations, 0.8))
    return

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

run()