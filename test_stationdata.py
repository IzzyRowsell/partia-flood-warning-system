# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the stationdata module"""

from floodsystem.stationdata import build_station_list, update_water_levels, inconsistent_typical_range_station
from floodsystem.station import MonitoringStation

def test_build_station_list():
    """Test building list of stations"""
    station_list = build_station_list()
    assert len(station_list) > 0


def test_update_level():
    """Test update to latest water level"""

    # Build list of stations
    stations = build_station_list()
    for station in stations:
        assert station.latest_level is None

    # Update latest level data for all stations
    update_water_levels(stations)
    counter = 0
    for station in stations:
        if station.latest_level is not None:
            counter += 1

    assert counter > 0


# Create stations for testing
s_id = "test-s-id"
m_id = "test-m-id"
label = "test station 1"
coord = (2, 3)
trange = (1,2)
river = "River X"
town = "Town 1"
s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 2"
trange = (2,3)
t = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 3"
trange = None
u = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 4"
trange = (-4,-20)
v = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 5"
trange = (17, 0)
w = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

label = "test station 6"
trange = (3,3)
x = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

def test_inconsistent_typical_range_station():

    station_list = [s,t,u]

    #normal input of 2 consistent stations and 1 none value
    a = inconsistent_typical_range_station(station_list)

    #normal input of all stations having inconsistent values
    b = inconsistent_typical_range_station([u,v,w])

    #input 3 stations which have consistent data
    c = inconsistent_typical_range_station([s,t,x])

    #testing multiple values
    d = inconsistent_typical_range_station([s,t,u,v,w,x])

    assert a == ['test station 3']
    assert b == ['test station 3', 'test station 4', 'test station 5']
    assert c == []
    assert d == ['test station 3', 'test station 4', 'test station 5']

