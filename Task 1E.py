from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

"""Run the rivers_by_station_number function in geo.py then prints the 9 rivers with the greatest number of monitoring stations"""

def run():
    stations = build_station_list()
    print(rivers_by_station_number(stations, 9))
    return

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")

run()
    