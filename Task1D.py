from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

"""Run the stations_by_distance function in geo.py then prints the 10 furthest and 10 closest stations from cambridge city centre"""

def run_1():
    stations = build_station_list()
    river_list = rivers_with_station(stations)
    print(len(river_list))
    print(sorted(river_list)[:10])
    return

def run_2():
    stations = build_station_list()
    river_dict = stations_by_river(stations)
    river_dict['River Aire'].sort()
    river_dict['River Cam'].sort()
    river_dict['River Thames'].sort()

    print(river_dict['River Aire'])
    print(river_dict['River Cam'])
    print(river_dict['River Thames'])
    return

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")

run_1()
run_2()