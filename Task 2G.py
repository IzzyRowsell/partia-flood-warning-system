from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    stations = build_station_list()
    update_water_levels(stations)
    rank = []
    town_dict = {}
    #rank the stations from highest to lowest relative level
    station_rank = stations_highest_rel_level(stations, len(stations))
    l = len(station_rank)
    
    #any station below average is considered low risk
    #any station upto 70% is medium risk
    #any station upto 40%  is high risk
    #any station in the top 5% is severe risk

    for i in range(l):
        if 0<= i <= l*0.05:
            rank.append(((station_rank[i])[0], "severe"))
            continue
        elif l*0.05 < i <= l*0.4:
            rank.append(((station_rank[i])[0], "high"))
            continue
        elif l*0.4 < i <= l*0.7:
            rank.append(((station_rank[i])[0], "medium"))
            continue
        else:
            rank.append(((station_rank[i])[0], "low"))
            continue
    
    for station in stations:
       for x in rank:
            if x[0] == station.name:
                town = station.town
                if (type(town) == str) and (town not in town_dict): 
                    town_dict[station.town] = x[1]
                elif (type(town) == str):
                    if x[1] == "severe":
                        town_dict[station.town] = x[1]
                    elif x[1] == "high" and (town_dict[station.town] == "medium" or town_dict[station.town] == "low"):
                        town_dict[station.town] = x[1]
                    elif x[1] == "medium" and (town_dict[station.town] == "low"):
                        town_dict[station.town] = x[1]


    print(town_dict)
    
    return    


if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")

run()